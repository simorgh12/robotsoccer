----StrategyB----------------------------------------
--------------------------------------------------
--BlueTeam------------------------------------------------
global i
global ftop, fbot, gRight, gLeft, fRightX,fLeftX, gtopy, gboty

global b1,b2,b3,b4,b5,ball,blist,pball,bpLast
global estat,estat1,estat2,estat3,ba1,ba2,bd1,bd2,bola5
global bantx,banty,angb,velb
--------------------

global migx,migy
global portax,portay,portdx,portdy
--------------------

-- Debug
global display

-- Blue goal
global goalBX, goalBY

-- Yellow goal
global goalYX, goalYY

-- Initial positions
global initB1

on strategyB
	-- Data initialization
	goalBX = fRightX
	goalBY = (gtopy + gboty) / 2
	goalYX = fLeftX
	goalYY = goalBY
	
	-- Initial robot positions
	initB1 = vector(90, 42, 0)
	initB2 = vector(82, 23, 0)
	initB3 = vector(82, 60, 0)
	initB4 = vector(62, 23, 0)
	initB5 = vector(62, 60, 0)
	
	-- Ball prediction
	predictBall()
	
	-- Debug
	goalie(b1, initB1)
	defender(b2, initB2)
	defender(b3, initB3)
	forward(b4, initB4)
	forward(b5, initB5)
end strategyB

-------------------------------------------------
--------------------Criteria---------------------
-------------------------------------------------
-- Distance between ball and robot
on disBallRob(trobot)
	pos = trobot.pos
	return(abs(ball.x-pos.x) + abs(ball.y-pos.y))
end disBallRob

-------------------------------------------------
--------------------Criteria---------------------
-------------------------------------------------
-- Distance between ball and blue goal
on disBallBlue()
	return(abs(ball.x-goalBX) + abs(ball.y-goalBY))
end disBallBlue

-------------------------------------------------
--------------------Criteria---------------------
-------------------------------------------------
-- Distance between ball and yellow goal
on disBallYellow()
	return(abs(ball.x-goalYX) + abs(ball.y-goalYY))
end disBallYellow

-------------------------------------------------
--------------------Criteria---------------------
-------------------------------------------------
-- Angular difference between goalB-ball and goalB-robot
-- If this difference is zero, robot is aligned with the ball and goal
-- If the difference is above zero, the ball above the robot
-- If the difference is below zero, the ball is below the robot
on angBlue(rob)
	d1x = goalBX - ball.x + 1e-9
	d1y = goalBY - ball.y 
	
	angGoalBall = atan(abs(d1y/d1x))

	d2x = goalBX - rob.pos.x + 1e-9
	d2y = goalBY - rob.pos.y 
	
	angGoalRob = atan(abs(d2y/d2x))
	
	outAngle = 0
	
	if ( d1y < 0 ) then
		if ( d2y < 0 ) then
			outAngle = angGoalBall - angGoalRob
		else
			outAngle = angGoalBall + angGoalRob
		end if
	else
		if ( d2y < 0 ) then
			outAngle = -(angGoalBall + angGoalRob)
		else
			outAngle = - angGoalBall + angGoalRob
		end if
	end if
	
	return(outAngle)
end angBlue

-------------------------------------------------
--------------------Criteria---------------------
-------------------------------------------------
-- Angular difference between goalY-ball and goalY-robot
-- If this difference is zero, robot is aligned with the ball and goal
-- If the difference is above zero, the ball above the robot
-- If the difference is below zero, the ball is below the robot
on angYellow(rob)
	d1x = ball.x - goalYX + 1e-9
	d1y = ball.y - goalYY
	
	angGoalBall = atan(abs(d1y/d1x))

	d2x = rob.pos.x - goalYX + 1e-9
	d2y = rob.pos.y - goalYY
	
	angGoalRob = atan(abs(d2y/d2x))
	
	outAngle = 0
	
	if ( d1y < 0 ) then
		if ( d2y < 0 ) then
			outAngle = angGoalBall - angGoalRob
		else
			outAngle = angGoalBall + angGoalRob
		end if
	else
		if ( d2y < 0 ) then
			outAngle = -(angGoalBall + angGoalRob)
		else
			outAngle = - angGoalBall + angGoalRob
		end if
	end if
	
	return(-outAngle)
end angYellow

-------------------------------------------------
--------------------Criteria---------------------
-------------------------------------------------
-- Distance between initial position and actual
on howfar(robot, initpos)
	return( abs( robot.pos.x - initpos.x ) + abs( robot.pos.y - initpos.y ) )
end howfar

-------------------------------------------------
---------------------Action----------------------
-------------------------------------------------
-- Blocks the ball, designed for goalie
on returnPosition(who, where)
	vesaxy(who, where.x, where.y, 1)
end returnPosition

-------------------------------------------------
---------------------Action----------------------
-------------------------------------------------
-- Blocks the ball, designed for goalie
-- Extracted from code_1_B
on blockBall(whichBot, where)
	T = ball  
	ypmax=52
	ypmin=32.3
	posx=where.x
	posy=pball.y

	dx=pball.x-ball.x +1e-9
	dy=pball.y-ball.y

	angle=atan(dy/dx)

	posy=pball.y-(pball.x-posx)*tan(angle)

	--Es posa davant la pilota
	-- This instruction may seem redundant, but it works well
	vesaxy(whichbot,posx,posy,1)

	--Si la pilota esta per sobre de la porteria

	if (posy > ypmax) then
		vesaxy(whichBot,posx,ypmax,1)
	end if

	--Si la pilota esta per sota de la porteria

	if (posy < ypmin) then
		vesaxy(whichBot,posx,ypmin,1)
	end if
	
end blockBall

-------------------------------------------------
---------------------Action----------------------
-------------------------------------------------
-- Block ball, but with freedom of movement
-- Modification of blockBall
on chaseBall(whichBot)
	posx=pball.x + 3-- This is a random threshold
	posy=pball.y

	dx=pball.x-ball.x +1e-9
	dy=pball.y-ball.y

	angle=atan(dy/dx)

	posy=pball.y-(pball.x-posx)*tan(angle)

	-- Go to the ball predicted location
	if ( pball.x > ball.x ) then
		vesaxy(whichbot,posx,posy,1)
	else
		vesaxy(whichbot,posx,posy,1)
	end if
	
end chaseBall

-------------------------------------------------
----------------------MCDSS----------------------
-------------------------------------------------
-- Goalie behaviour
on topsisGoalie(who, where)
	
	-- Distance threshold
	thres = 70
	
	-- pCA -> C = Criteria, A = Alternative
	-- iA -> Ideal vs Alternative
	-- nA -> Nadir vs Alternative
	------ Criterias
	-- 1 = distance robot to initial position
	-- 2 = distance ball to blue goal
	------ Alternatives
	-- 1 = return to initial position
	-- 2 = block ball
	-------------------------------------------
	-- Distance to initial position performances
	dis = howfar(who, where)
	-- Return to initial position, proportional
	if ( dis > thres ) then
		p11 = 1
	else
		p11 = dis/thres
	end if
	i1 = (1-p11)*(1-p11)
	n1 = (0-p11)*(0-p11)
	
	-- Block ball, inversely proportional
	p12 = 1 - p11
	i2 = (1-p12)*(1-p12)
	n2 = (0-p12)*(0-p12)
	-------------------------------------------
	
	-- Distance of the ball to the blue goal
	dis = disBallBlue()
	-- Return to initial position, proportional
	if ( dis > thres ) then
		p21 = 1
	else
		p21 = dis/thres
	end if
	i1 = i1 + (1-p21)*(1-p21)
	n1 = n1 + (0-p21)*(0-p21)
	
	-- Block ball, inversely proportional
	p22 = 1 - p21
	i2 = i2 + (1-p22)*(1-p22)
	n2 = n2 + (0-p22)*(0-p22)
	-------------------------------------------
	
	-- Calculating distances to virtual actions
	i1 = sqrt(i1)
	n1 = sqrt(n1)
	i2 = sqrt(i2)
	n2 = sqrt(n2)
	-------------------------------------------
	
	-- Calculating closeness coeficient
	c1 = n1 / ( i1 + n1 )
	c2 = n2 / ( i2 + n2 )
	
	if ( c1 > c2 ) then
		return (1)
	else 
		return (2)
	end if
	
	return(4)

end topsisGoalie

-- Defender behaviour
on topsisDefender(who, where)
	
	-- Distance threshold
	thres = 70
	
	-- pCA -> C = Criteria, A = Alternative
	-- iA -> Ideal vs Alternative
	-- nA -> Nadir vs Alternative
	------ Criterias
	-- 1 = distance robot to initial position
	-- 2 = distance ball to blue goal
	-- 3 = distance ball to robot
	------ Alternatives
	-- 1 = return to initial position
	-- 2 = block ball
	-- 3 = clear ball
	-------------------------------------------
	-- Distance to initial position performances
	dis = howfar(who, where)
	-- Return to initial position, proportional
	if ( dis > thres ) then
		p11 = 1
	else
		p11 = dis/thres
	end if
	i1 = (1-p11)*(1-p11)
	n1 = (0-p11)*(0-p11)
	
	-- Block ball, inversely proportional
	p12 = 1 - p11
	i2 = (1-p12)*(1-p12)
	n2 = (0-p12)*(0-p12)
	
	-- Clear ball, the same as block ball
	p13 = p12
	i3 = (1-p13)*(1-p13)
	n3 = (0-p13)*(0-p13)
	-------------------------------------------
	
	-- Distance of the ball to the blue goal
	dis = disBallBlue()
	-- Return to initial position, function
	-- If too near, one (evade scoring in our own goal)
	-- If too far, one, we want to stay in place
	-- Midrage is 1
	if ( dis > 70 ) then
		p21 = 1
	else if (dis < 10) then 
		p21 = 1
	else if ( dis >= 40) then 
		p21 = (dis-40)/30
	else
		p21 = (40-dis)/30
	end if
	i1 = i1 + (1-p21)*(1-p21)
	n1 = n1 + (0-p21)*(0-p21)
	
	-- Block ball, function 
	--> zero when too far or too near the goal (to evade scoring in our own goal)
	--> one at a midpoint
	if ( dis > 90 ) then
		p22 = 0 -- Too far
	else if ( dis < 20 ) then
		p22 = 0 -- Too near
	else if ( dis >= 55) then
		p22 = 1 - (dis-55)/35 -- Increasing section
	else
		p22 = 1 - (55-dis)/35 -- Decreasing section
	end if
	i2 = i2 + (1-p22)*(1-p22)
	n2 = n2 + (0-p22)*(0-p22)
	
	-- Clear ball, the same as block ball
	p23 = p22
	i3 = i3 + (1-p23)*(1-p23)
	n3 = n3 + (0-p23)*(0-p23)
	-------------------------------------------
	
	-- Distance of the ball to the robot
	dis = disBallRob(who)
	-- Return to initial position, proportional
	if ( dis > thres ) then
		p31 = 1
	else
		p31 = dis/thres
	end if
	i1 = i1 + (1-p31)*(1-p31)
	n1 = n1 + (0-p31)*(0-p31)
	
	-- Block ball, function 
	--> zero when too far or too near the goal (to evade scoring in our own goal)
	--> one at a midpoint
	if ( dis > 70 ) then
		p32 = 0 -- Too far
	else if ( dis < 10 ) then
		p32 = 0 -- Too near
	else if ( dis >= 40) then
		p32 = 1 - (dis-40)/30 -- Increasing section
	else
		p32 = 1 - (40-dis)/30 -- Decreasing section
	end if
	i2 = i2 + (1-p32)*(1-p32)
	n2 = n2 + (0-p32)*(0-p32)
	
	-- Clear ball, function
	-- Zero when too far
	-- One when clossest
	if ( dis > 40 ) then
		p33 = 0 -- Too far
	else if ( dis < 10 ) then
		p33 = 1 -- Too near
	else
		p33 = 1 - (dis-10)/30 -- Increasing section
	end if
	i3 = i3 + (1-p33)*(1-p33)
	n3 = n3 + (0-p33)*(0-p33)
	-------------------------------------------
	
	-- Calculating distances to virtual actions
	i1 = sqrt(i1)
	n1 = sqrt(n1)
	i2 = sqrt(i2)
	n2 = sqrt(n2)
	i3 = sqrt(i3)
	n3 = sqrt(n3)
	-------------------------------------------
	
	-- Calculating closeness coeficient
	c1 = n1 / ( i1 + n1 )
	c2 = n2 / ( i2 + n2 )
	c3 = n3 / ( i3 + n3 )
	
	if ( c1 > c2 ) then
		if ( c1 >= c3 ) then
			return (1)
		end if
	end if
	
	if ( c2 > c3 ) then
		return (2)
	else
		return (3)
	end if
	
end topsisDefender

-- Forward behaviour
on topsisForward(who, where)
	
	-- Distance threshold
	thres = 120
	
	-- pCA -> C = Criteria, A = Alternative
	-- iA -> Ideal vs Alternative
	-- nA -> Nadir vs Alternative
	------ Criterias
	-- 1 = distance robot to initial position
	-- 2 = distance ball to blue goal
	-- 3 = distance ball to robot
	------ Alternatives
	-- 1 = return to initial position
	-- 2 = block ball
	-- 3 = attack ball
	-------------------------------------------
	-- Distance to initial position performances
	dis = howfar(who, where)
	-- Return to initial position, proportional
	if ( dis > thres ) then
		p11 = 1
	else
		p11 = dis/thres
	end if
	i1 = (1-p11)*(1-p11)
	n1 = (0-p11)*(0-p11)
	
	-- Block ball, inversely proportional
	p12 = 1 - p11
	i2 = (1-p12)*(1-p12)
	n2 = (0-p12)*(0-p12)
	
	-- Attack ball, the same as block ball
	p13 = p12
	i3 = (1-p13)*(1-p13)
	n3 = (0-p13)*(0-p13)
	-------------------------------------------
	
	-- Distance of the ball to the blue goal
	dis = disBallBlue()
	-- Return to initial position, inversely proportional
	if ( dis > thres) then
		p21 = 1
	else
		p21 = 1 - dis / thres
	end if
	i1 = i1 + (1-p21)*(1-p21)
	n1 = n1 + (0-p21)*(0-p21)
	
	-- Block ball, function 
	--> zero when the ball is near the yellow goal and when it is past the defenders defense line
	--> one at a midpoint
	if ( dis > 90 ) then
		p22 = 0 -- Too far
	else if ( dis < 50 ) then
		p22 = 0 -- Too near
	else if ( dis >= 70) then
		p22 = 1 - (dis-70)/20 -- Increasing section
	else
		p22 = 1 - (70-dis)/20 -- Decreasing section
	end if
	i2 = i2 + (1-p22)*(1-p22)
	n2 = n2 + (0-p22)*(0-p22)
	
	-- Attack ball, inversely proportional
	if ( dis > 90 ) then
		p23 = 1
	else
		p23 = 1 - dis/90
	end if
	i3 = i3 + (1-p23)*(1-p23)
	n3 = n3 + (0-p23)*(0-p23)
	-------------------------------------------
	
	-- Distance of the ball to the robot
	dis = disBallRob(who)
	-- Return to initial position, proportional
	if ( dis > thres ) then
		p31 = 1
	else
		p31 = dis/thres
	end if
	i1 = i1 + (1-p31)*(1-p31)
	n1 = n1 + (0-p31)*(0-p31)
	
	-- Block ball, indifferent, when triggered by other criteria
	p32 = 0.5
	i2 = i2 + (1-p32)*(1-p32)
	n2 = n2 + (0-p32)*(0-p32)
	
	-- Clear ball, function
	-- Zero when too far
	-- One when clossest
	if ( dis > 40 ) then
		p33 = 0 -- Too far
	else if ( dis < 10 ) then
		p33 = 1 -- Too near
	else
		p33 = 1 - (dis-10)/30 -- Increasing section
	end if
	i3 = i3 + (1-p33)*(1-p33)
	n3 = n3 + (0-p33)*(0-p33)
	-------------------------------------------
	
	-- Calculating distances to virtual actions
	i1 = sqrt(i1)
	n1 = sqrt(n1)
	i2 = sqrt(i2)
	n2 = sqrt(n2)
	i3 = sqrt(i3)
	n3 = sqrt(n3)
	-------------------------------------------
	
	-- Calculating closeness coeficient
	c1 = n1 / ( i1 + n1 )
	c2 = n2 / ( i2 + n2 )
	c3 = n3 / ( i3 + n3 )
	
	if ( c1 > c2 ) then
		if ( c1 >= c3 ) then
			return (1)
		end if
	end if
		
	if ( c2 > c3 ) then
		return (2)
	else
		return (3)
	end if

end topsisForward

-------------------------------------------------
-------------------Behaviour---------------------
-------------------------------------------------
------------------->Goalie<----------------------
on goalie(who, where)
	
	action = topsisGoalie(who, where)
	--display = action
	
	if ( action = 1 ) then
		returnPosition(who, where)
	else if ( action = 2 ) then 
		blockBall(who, where)
	end if
end goalie

-------------------------------------------------
-------------------Behaviour---------------------
-------------------------------------------------
------------------>Defender<---------------------
on defender(who, where)
	
	action = topsisDefender(who, where)
	--display = action
	
	if ( action = 1 ) then
		returnPosition(who, where)
	else if ( action = 2 ) then 
		chaseBall(who, where)
	else if ( action = 3 ) then
		atacabola(who)
	end if
	
end defender

-------------------------------------------------
-------------------Behaviour---------------------
-------------------------------------------------
------------------->Forward<---------------------
on forward(who, where)
	
	action = topsisForward(who, where)
	display = action
	
	if ( action = 1 ) then
		returnPosition(who, where)
	else if ( action = 2 ) then 
		chaseBall(who, where)
	else if ( action = 3 ) then
		atacabola(who)
	end if
	
end forward

---------------predictBall-----------------------
-------Prediu posicio de pilota------------------

-------------------------------------------------

on predictBall
  
	k=4
	blast = getAt(blist, i - 1)
	pX = ball.x + k*(ball.x - blast.x)
	pY = ball.y + k*(ball.y - blast.y)
	pBall = vector(pX,pY,0)

end predictball

-------------------------------------------------



-------------------------------------------------

---------------tanh(a)---------------------------
----------calcuala la tanh-----------------------

-------------------------------------------------

on tanh(a)

	out=(exp(a)-exp(-a))/(exp(a)+exp(-a)+1e-9)
	return(out)

end tanh

-------------------------------------------------




-------------------------------------------------

---------------combinacio(f1,fo,x,xo)------------
----combina dues funcions pel metode de tanh-----

-------------------------------------------------

on combinacio(f1,f2,x,xo)

	--xo=30
	c=0.05
	f=((f2-f1)/2)*tanh(c*(x-xo))+ (f2+f1)/2
	return(f)

end combinacio


-------------------------------------------------




-------------------------------------------------

------------aplica(who,vlin,w)-------------------
----------aplica les velocitats a cada roda------

-------------------------------------------------


on aplica(who,vlin,w)

	vl= vlin - (3/2)*w
	vr= vlin + (3/2)*w

	velocity(who,vl,vr)

end aplica

-------------------------------------------------




-------------------------------------------------

---------------vesaxy(whichBot,posx,posy,rot)----
---------------Va a posicio (posx,posy)----------

-------------------------------------------------

on vesaxy(whichBot,posx,posy,rot)

	ka=3
	kd=15

	if (rot=1) then
		kd=9
		ka=2.5
	end if

	A = whichBot.pos
	R = whichBot.rot.z 

	--Calcula distancia i angle a la pilota--	

	dX = A.x - posx + 1e-9
	dy = A.y - posy 

	dist = sqrt(dx*dx + dy*dy)
	
	angleToPoint = atan(abs(dY/dX)) * 180.0/pi()

 

	--Ajusta resultat al quadrant corresponent-
	

        if(dX > 0) then
           if(dY > 0) then
    		angleToPoint = angleToPoint - 180
       	    else  if(dY < 0) then 
  		angleToPoint = 180.0 - angleToPoint
      	    end if 
 	end if
  	if(dX < 0) then
            if(dY > 0) then
	    angleToPoint = 180 -angleToPoint -180
	    else  if(dY < 0) then 
	    angleToPoint = 270 - angleToPoint -180
	    end if      
	end if 

	angdist=angleToPoint-R

	if (angdist>180) then
		angdist=angdist-360
	end if
	if (angdist<-180) then
		angdist=angdist+360
	end if


	--Si pilota esta per darrera el robot
	--aquet invertira la seva direccio
	
	signe=1	
	if (angdist>90) then
		angdist=180-angdist
		signe=-1
	end if
	if (angdist<-90) then
		angdist=-180-angdist
		signe=-1
	end if

	
	f2=ka*angdist
	f1=0
	
	w=combinacio(f1,f2,dist,30)
	w=signe*W	

	f2=0
	f1=kd*dist
	

	vlin=combinacio(f1,f2,abs(angdist),45)
	vlin=signe*vlin
	
	aplica(whichBot,vlin,w)


end vesaxy
-------------------------------------------------


-------------------------------------------------

---------------Calcaprox(who)--------------------
---------------Aproxima a la bola----------------

-------------------------------------------------

on calcaprox(who)

	

	dx=pball.x-portax + 1e-9
	dy=pball.y-portay

	angle=atan(dy/dx)

	apxx=5*cos(angle)+pball.x
	apxy=5*sin(angle)+pball.y

	dax = apxx - who.pos.x
	day = apxy - who.pos.y 
	dista = sqrt(dax*dax + day*day)

	vesaxy(who,apxx,apxy,0)

	if (dista<3) then
	    r=1
	else
	    r=0
	end if

	return(r)
end calcaprox


-------------------------------------------------


-------------------------------------------------

---------------Calcaproxdef(who)-----------------
---------------Aproxima a la bola----------------

-------------------------------------------------

on calcaproxdef(who)

	

	dx=portdx-pball.x + 1e-9
	dy=portdy-pball.y

	angle=atan(dy/dx)

	apxx=10*cos(angle)+pball.x
	apxy=10*sin(angle)+pball.y



	dax = apxx - who.pos.x
	day = apxy - who.pos.y 
	dista = sqrt(dax*dax + day*day)


	vesaxy(who,apxx,apxy,0)

	if (dista<2) then
	    r=1
	else
	    r=0
	end if

	return(r)
end calcaproxdef


-------------------------------------------------



---------------Tapad(who,d)----------------------
---------------Aproxima a la bola----------------

-------------------------------------------------

on Tapad(who,d)

	portx=fRightX
	porty=fbot+35.4

	dx=portx-ball.x +1e-9
	dy=porty-ball.y

	angle=atan(dy/dx)

	apxx=portx-d
	apxy=porty-d*tan(angle)

	dax = apxx - who.pos.x
	day = apxy - who.pos.y 
	dista = sqrt(dax*dax + day*day)

	vesaxy(who,apxx,apxy,0)

end Tapad


-------------------------------------------------



-------------------------------------------------

---------------atacabola(who)--------------------
-----------corre cap a la porteria---------------

-------------------------------------------------

on atacabola(who)

	ka=1
	kd=11

	A = who.pos
	R = who.rot.z 

	
	-------get angleToPoint-----

	dx = A.x - ball.x + 1e-9
	dy = A.y - ball.y 

	dist = sqrt(dx*dx + dy*dy)
	
	angleToPoint = atan(abs(dy/dx)) * 180.0/pi()

 
	--Adjust result for quadrants-------------------------------
	--eg above left, above right, below left, below right-------

    	if(dx > 0) then
        	if(dy > 0) then
    	        	angleToPoint = angleToPoint - 180
        	else  if(dy < 0) then 
  	        	angleToPoint = 180.0 - angleToPoint
        	end if 
 	end if
  	if(dx < 0) then
            if(dy > 0) then
	    angleToPoint = 180 -angleToPoint -180
	    else  if(dy < 0) then 
	    angleToPoint = 270 - angleToPoint -180
	    end if      
	end if 

	angdist=angleToPoint-R

	if (angdist>180) then
		angdist=angdist-360
	end if
	if (angdist<-180) then
		angdist=angdist+360
	end if

	signe=1	

	-----------------------------------------------------------

	--Calcula en quin sentit arribara mes rapid----------------	

	if (angdist>90) then
		angdist=180-angdist
		signe=-1
	end if
	if (angdist<-90) then
		angdist=-180-angdist
		signe=-1
	end if
	----------------------------------------------------------

	w=ka*angdist
	w=signe*w
	

	vlin=signe*100
	
	aplica(who,vlin,w)
	if (dist<1) then
		r=1
	else
		r=0
	end if
	return(r)

end atacabola


-------------------------------------------------


-------------------------------------------------

---------------Goalie1(who)----------------------
---------------Porter----------------------------

-------------------------------------------------

on Goalie1(whichBot)

T = ball  
ypmax=52
ypmin=32.3
posx=90.4
posy=pball.y





	dx=pball.x-ball.x +1e-9
	dy=pball.y-ball.y

	angle=atan(dy/dx)

	
	posy=pball.y-(pball.x-posx)*tan(angle)




--Es posa davant la pilota

vesaxy(whichbot,posx,posy,1)

--Si la pilota esta per sobre de la porteria

if (posy > ypmax) then
	posy = ypmax
	vesaxy(whichBot,posx,posy,1)
end if

--Si la pilota esta per sota de la porteria

if (posy < ypmin) then
	posy=ypmin
	vesaxy(whichBot,posx,posy,1)

end if


--Si la pilota esta mes lluny de mig del camp

if (pball.x < fLeftX+43.3) then
	posy = 42.15
	vesaxy(whichBot,posx,posy,1)
end if




end

-------------------------------------------------




-------------------------------------------------

---------------defend1(who)----------------------
---------------defensa1--------------------------

-------------------------------------------------


on defend1(whichBot)

ypmax= fbot+35.5
portx=fLeftX
porty=fbot+35.4
 
   
if (ball.x > fLeftX+43.3) then
	
	--Si esta en el seu quadrant s'apropa i ataca la bola

	if (ball.y < fbot+35.4) then
	     	
 	     
	 dbx = ball.x - whichBot.pos.x
         dby = ball.y - whichBot.pos.y 
         distb = sqrt(dbx*dbx + dby*dby)

         if (estat=0) then
                  
                  atacabola(whichBot)
                  if (distb>25) then
		  	estat=1
                  end if
         end if


        if (estat=1) then
		   da=calcaproxdef(whichbot)
		   if (da=1) then
		         estat=0
		   end if
        
        end if
        
	end if

	
	--Si esta en la zona del altre defensa

	if (ball.y > fbot+35.4) then
		posy=migy+6
		posx=ball.x
		vesaxy(whichbot,posx,posy,0)
	end if

end if


--Si esta mes enlla del mig del camp

if (ball.x < fLeftX+43.3) then
	posx=fLeftX+65
	posy=ball.y
	if (ball.y > ypmax) then
		posy = ypmax
		vesaxy(whichbot,posx,posy,0)
	else
		tapad(whichbot,30)
	end if
	
	
end if


end defend1



-------------------------------------------------

---------------defend2(who)----------------------
---------------defensa2--------------------------

-------------------------------------------------


on defend2(whichBot)

ypmax= fbot+35.5
  if (ball.x > fLeftX+43.3) then
	if (ball.y > fbot+35.4) then
	     	 	    
	 dbx = ball.x - whichBot.pos.x
         dby = ball.y - whichBot.pos.y 
         distb = sqrt(dbx*dbx + dby*dby)

         if (estat1=0) then
                  
	         atacabola(whichBot)
                 if (distb>25) then
		  	estat1=1
                 end if
         end if


	if (estat1=1) then
		da=calcaprox(whichbot)
		if (da=1) then
	        	estat1=0
		end if
        end if 
     
   end if

	if (ball.y < fbot+35.4) then
		--posy=ball.y+30
		posy=migy-6
		posx=ball.x
		vesaxy(whichbot,posx,posy,0)
	end if

end if
if (ball.x < fLeftX+43.3) then
	posx=fLeftX+65
	posy=ball.y
	if (ball.y < ypmax) then
		posy = ypmax
		vesaxy(whichbot,posx,posy,0)
	else
		tapad(whichbot,30)
	end if
	
end if
end


-------------------------------------------------

---------------attack2(who)--------------------
---------------Davanter2----------------

-------------------------------------------------


on attack2(whichBot)
	
	
	
	if (ball.x < migx) then

		dbx = ball.x - whichBot.pos.x
        	dby = ball.y - whichBot.pos.y 
  		dstb = sqrt(dbx*dbx + dby*dby)
		
		--Si la pilota la te l'altre davanter		

		if (ba1=1) then
		
			--S'en va a posici� de remat segons on estigui la bola			

			if (ball.x < FLeftX+10) then
				
				if (ball.y < migy) then
					posy=portay+7
				else
					posy=portay-7
				end if

				posx=ball.x+4
			
			else
				
				posx=portax+15
				posy=portay
			
			end if

			--Si la pilota esta a prop ataca la pilota

			if (dstb<10) then
				if (ball.x<posx) then
					atacabola(whichbot)
				else
					vesaxy(whichbot,posx,posy,0)
				end if
			else
				vesaxy(whichbot,posx,posy)
			end if
		
		-- Si la pilota no la te l'altre atacant va ell a buscarla
	
		else

			if (estat3=0) then
        	       		atacabola(whichBot)
                	  	if (dstb>25) then
					estat3=1
	               		end if
				
         		end if


	        	if (estat3=1) then
				da=calcaprox(whichbot)
			        if (da=1 ) then
				         estat3=0
				end if
                	
			end if
		
		
		end if 
		
	end if


	if (ball.x >= migx) then
		posx=migx
		if (ball.y<migy) then
			posy=migy+15
		else
			posy=migy-15
		end if
		vesaxy(whichbot,posx,posy,0)
	end if

end

-------------------------------------------------

-------------------------------------------------

---------------attack1(who)----------------------
---------------davanter1-------------------------

-------------------------------------------------

on attack1(whichBot)
  

	
	

		dbx = ball.x - whichBot.pos.x
        	dby = ball.y - whichBot.pos.y 
  		dstb = sqrt(dbx*dbx + dby*dby)
		
		--Si esta a prop de la pilota activa ba1
		
		if (dstb<30) then
			ba1=1
		else
			ba1=0
		end if

		--S'aproxima i ataca la bola per tot el camp		

		if (estat2=0) then
               		atacabola(whichBot)
                  	if (dstb>15) then
				--ba1=0
				estat2=1
	               	end if
			
         	end if


        	if (estat2=1) then
			da=calcaprox(whichbot)
		        if (da=1 ) then
			        --ba1=1 
				estat2=0
			end if
                
		end if
		
		
	
	
		

	
  	
end
------------------------------------------------

