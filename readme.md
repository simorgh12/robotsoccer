RobotSoccer - Master branch
=================================================

### Final project for Cooperative Robotics (CR) along with Multi-Criteria Decision Support Systems (MCDSS) courses from the Master in Artificial Intelligence (MAI) at UPC.

### Contributors:
* Luis Fábregues (luis.fabregues@est.fib.upc.edu)
* Vicent Roig (vicent.roig@est.fib.upc.edu)

----------------------------------------------------------

#### Meaning of file's name "code_#_X"
You have 6 different examples of code: code file + PPT with some detailed explanations.
Most of the code samples are related to Yellow team (code_#_Y), but only one relates to Blue team (code_1_B).
Detailed description:
* code_1_B: full Blue team (2 stupid deffend robots)
* code_2_Y: full Yellow team
* code_3_Y: only 1 attack robot (yellow)
* code_4_Y: 2 attack robots (yellow)
* code_5_Y: 2 deffend + goalkeeper (yellow)
* code_6_Y: only goalkeeper robot (yellow)

#### Installation of the simulator (only Windows)
Just execute EXE file and wait for a while.

#### Rules (only for official competition)
See PDF file. Just for more info.

####CPP folder
Alternatively, you could write your strategy in CPP language.
CPP folder shows an example of "how to": variables, simulator data structures, ...

Remember: in compiler options you must specify DLL as output file (not EXE file).

----------------------------------------------------------
