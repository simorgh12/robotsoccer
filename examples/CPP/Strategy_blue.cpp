// Strategy.cpp : Defines the entry point for the DLL application.

//Porter: 85%
//Defensa1: 70%
//Defensa 2: 0%
//Atack1: 0%
//Atack2: 0%
//esquivar altres


#include "stdafx.h"
#include "Strategy.h"

#include <math.h>
#include <stdio.h>

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}



typedef struct
{
	double vl, vr;
} Velocitats;

const double PI = 3.1415923;
/////////Constants////////////

const double MIGCAMPX = ((FRIGHTX - FLEFTX) / 2)+FLEFTX;
const double MIGCAMPY = ((FTOP - FBOT) / 2)+FBOT;

//zones camp
const int ZONA_DEF = 0;
const int ZONA_ATAC = 1;
const int ZONA_NEUTRE=10;

//elements del camp
const int ELEM_PILOTA = 0;
const int ELEM_ROBOT = 1;

//rols robots
const int ROL_DEF_PORT=0;
const int ROL_DEF_CAMP=1;

const int ROL_ATAC_CENTRE=2;
const int ROL_ATAC_GOL=3;

const int ROL_PORTER=4;

//Posicio porter

const double POSX_PORTER=90.468;
const double POSY_PORTER=47;
const double ROT_PORTER=-90;

//Posicio defensa
const double POSX_DEF=83;
const double PENAL=75;
const double GRtoRAD= PI/180;


FILE *log_p;

int rol_rbts[] = {4,0,0,3,3};

void inicialitzacions(Environment *env);
void PredictBall ( Environment *env );
void Porter ( Robot *robot, Environment *env );
void NearBound2 ( Robot *robot, double vl, double vr, Environment *env , int qui);
void Attack1 ( Robot *robot, Environment *env , int qui);
void Attack2 ( Robot *robot, Environment *env , int qui);
void Defend1 ( Robot *robot, Environment *env, double low, double high , int qui);
void Defend2 ( Robot *robot, Environment *env, double low, double high , int qui);

///////////////////

void vesaxy (Robot *robot, double posx, double posy, double rot, Environment, int qui);
void assignaRols ( Environment *env );
int quinRol(Environment *env, int num);



extern "C" STRATEGY_API void Create ( Environment *env )
{
	// allocate user data and assign to env->userData
	// eg. env->userData = ( void * ) new MyVariables ();
}

extern "C" STRATEGY_API void Destroy ( Environment *env )
{
	// free any user data created in Create ( Environment * )

	// eg. if ( env->userData != NULL ) delete ( MyVariables * ) env->userData;
}


extern "C" STRATEGY_API void Strategy ( Environment *env )
{
	errno_t err;
	if( (err  = fopen_s( &log_p, "C:/strategy/log.txt", "a" )) !=0 )
		printf( "The file 'log.txt' was not opened\n" );
	else
		printf( "The file 'log.txt' was opened\n" );
	//fopen_s(log,"log.txt","a");
	fprintf(log_p,"###################### comencem ##################\n");
	inicialitzacions(env);

//	fprintf(log_p,"gameState: %lg\n",env->gameState);
	PredictBall ( env );
	switch (env->gameState)
	{
		case FREE_BALL:
			fprintf(log_p,"FREE_BALL\n");
			assignaRols(env);
			break;

		case PLACE_KICK:
			fprintf(log_p,"PLACE_KICK\n");
			//vesaxy (&env->home [ 0 ], 30, 30, 90);
			assignaRols(env);
			break;			
		case PENALTY_KICK:
			fprintf(log_p,"PENALTY_KICK\n");
			//vesaxy (&env->home [ 0 ], 40, 40, 90);
			switch (env->whosBall)
			{
			case ANYONES_BALL:
				assignaRols(env);
				break;
			case BLUE_BALL:
				assignaRols(env);
				break;
			case YELLOW_BALL:
				assignaRols(env);
				break;
			}

		case FREE_KICK:
			fprintf(log_p,"FREE_KICK\n");
			//vesaxy (&env->home [ 0 ], 50, 50, 90);
			assignaRols(env);
			break;
		case GOAL_KICK:
			fprintf(log_p,"GOAL_KICK\n");
			//vesaxy (&env->home [ 0 ], 60, 60, 90);
			assignaRols(env);
			break;
		default:
			fprintf(log_p,"NO_STATE\n");
			//vesaxy (&env->home [ 0 ], 60, 60, 90);
			assignaRols(env);
    }
	fprintf(log_p,"###################### acabem ##################\n");
	fclose(log_p);
}


void inicialitzacions(Environment *env){
	
	env->fieldBounds.bottom=FBOT;
	env->fieldBounds.left=FLEFTX;
	env->fieldBounds.right=FRIGHTX;
	env->fieldBounds.top=FTOP;
	env->goalBounds.bottom=GBOTY;
	env->goalBounds.left=GLEFT;
	env->goalBounds.right=GRIGHT;
	env->goalBounds.top=GTOPY;
	
	fprintf(log_p,"fb %g, fl %g, fr %g, ft %g\n",env->fieldBounds.bottom,env->fieldBounds.left,env->fieldBounds.right,env->fieldBounds.top);
	fprintf(log_p,"gb %g, gl %g, gr %g, gt %g\n",env->goalBounds.bottom,env->goalBounds.left,env->goalBounds.right,env->goalBounds.top);

}


// ens diu a kina zona del terreny hi ha la pilota 

int quinaZona(Environment *env, int qui, int rol){ // per equip blau: qui-> robot o pilota
	
	double dx = env->predictedBall.pos.x;
	double dy = env->predictedBall.pos.y;
	int zona= ZONA_DEF;
	//fprintf(log_p,"quinaZona: qui: %d, dx: %g, migcamp: %g\n",qui,dx,MIGCAMP);
	if (qui == ELEM_PILOTA){
	//	fprintf(log_p," hola 1");
		if(rol == ROL_DEF_PORT || rol == ROL_DEF_CAMP){
			if(dx > MIGCAMPX+ZONA_NEUTRE){  // pilota en la zona de defensa
				zona= ZONA_DEF;
			}else{
				zona= ZONA_ATAC;
			}
		}else if(rol == ROL_ATAC_CENTRE || rol == ROL_ATAC_GOL ){
			if(dx > MIGCAMPX-ZONA_NEUTRE){
				zona= ZONA_DEF;
			}else{
				zona= ZONA_ATAC;
			}
		}
	}else if (qui == ELEM_ROBOT){
		//fprintf(log_p," hola 2");
		if(dx > MIGCAMPX){
			zona= ZONA_DEF;
		}else{
			zona= ZONA_ATAC;
		}
	}
	return zona;
}

/* Ens diu quin rol ha de tenir depenent de la posici� de la pilota, i de la posici� on �s ell ara
 Rols defensa pot ser: 
	- Protegir Porteria
	- Treure la pilota de la zona de perill
 Rols atac:
	- Centre campista, evitar que la pilota passi de mig camp
	- Delanter: Fer gols
*/

int quinRol(Environment *env, int num,int rolAnt){ // per equip blau: 
	
	int zona=quinaZona(env,ELEM_PILOTA, rolAnt);
	int rol=0;
	//fprintf(log_p,"Robot num: %d zona: %d\n",num,zona);
	if(num==1 || num==2){ //defenses
		if (zona==ZONA_DEF){
			rol=ROL_DEF_PORT;
		}else{
			rol=ROL_DEF_CAMP;
		}
	}else if(num==3 || num==4){ //delanters
		if (zona==ZONA_DEF){
			rol=ROL_ATAC_CENTRE;
		}else{
			rol=ROL_ATAC_GOL;
		}
	}else{   // es el porter
		rol=ROL_PORTER;
	}
//	fprintf(log_p,"Rooooooool: %d\n",rol);
	return rol;
}

void assignaRols(Environment *env){
	//vesaxy (&env->home [ 1 ], 20, 20, 90);

	fprintf(log_p,"fb %g, fl %g, fr %g, ft %g\n",env->fieldBounds.bottom,env->fieldBounds.left,env->fieldBounds.right,env->fieldBounds.top);
	fprintf(log_p,"gb %g, gl %g, gr %g, gt %g\n",env->goalBounds.bottom,env->goalBounds.left,env->goalBounds.right,env->goalBounds.top);


	int rol=0;
	for (int i = 0; i < 5; i++){
		rol=quinRol(env, i, rol_rbts[i]);
		//fprintf(log_p,"Robot: %d rol: %d\n",i,rol);
		switch (rol)
		{
		case ROL_DEF_PORT:
			rol_rbts[i]=ROL_DEF_PORT;
			if(i==1){
				Defend1 ( &env->home [ i ], env, 0, 35 , i);
			}else{
				Defend1 ( &env->home [ i ], env, 40, 60 , i);
			}
			break;
		case ROL_DEF_CAMP:
			rol_rbts[i]=ROL_DEF_CAMP;
			if(i==1){
				Defend2 ( &env->home [ i ], env, 0, 35 , i);
			}else{
				Defend2 ( &env->home [ i ], env, 40, 60 , i);
			}
			break;
		case ROL_ATAC_CENTRE:
			rol_rbts[i]=ROL_ATAC_CENTRE;
			Attack1 ( &env->home [ i ], env , i);
			break;
		case ROL_ATAC_GOL:
			rol_rbts[i]=ROL_ATAC_GOL;
			Attack2 ( &env->home [ i ], env , i);
			break;
		case ROL_PORTER:
			rol_rbts[i]=ROL_PORTER;
			Porter ( &env->home [ i ], env );
			break;
		}
	}
	//vesaxy (&env->home [ 2 ], 40, 40, 90);
}
//Calcula la tanh

double tanh2 (double a){

	double out = (exp(a) - exp(-a))/(exp(a) + exp(-a) +1e-9);
	return out;
}


//Combina dues funcions pel metode d tanh

double combinacio (double f1, double f2, double x, double xo){

	double c = 0.05;
	double f = ((f2 - f1) / 2) * tanh(c * (x - xo)) + (f2 + f1) / 2;
	return (f);

}




// aplica les velocitats a cada roda

void aplica (Robot *who, double vlin, double w, Environment *env, int qui){
	
	double vl = vlin - (3/2)*w;
	double vr = vlin + (3/2)*w;
	fprintf(log_p,"vl: %g vr: %g \n",vl,vr);
	//velocity(who,vl,vr);
	NearBound2 ( who, vl ,vr, env ,qui);
	who->velocityLeft = vl;
	who->velocityRight = vr;

}



// Va a posici� (posx,posy)


void vesaxy (Robot *robot, double posx, double posy, double rot, Environment *env, int qui){

	float ka = 3;
	float kd = 15;

	if (rot==1){
		kd=9;
		ka=2.5;
	}

	Vector3D A = robot->pos;
	double R = robot->rotation;

	//Calcula distancia i angle a la pilota

	double dx = A.x - posx +1e-9;
	double dy = A.y - posy +1e-9;
	
	double dist = sqrt(dx*dx + dy*dy);
	//fprintf(log_p,"dx: %g (A.y:%g - posy:%g = dy: %g) dist: %g\n",dx,A.y,posy,dy,dist);
	double angleToPoint = atan(abs(dy/dx)) * (180/ PI);

	// Ajusta resultat al quadrant corresponent

	if(dx > 0){
		if(dy > 0){
			angleToPoint -= 180;
		}else if (dy < 0){
			angleToPoint = 180 - angleToPoint;
		}
	}
	if(dx < 0){
		if(dy > 0){
			angleToPoint = 180- angleToPoint - 180;
		}else if (dy < 0){
			angleToPoint = 270 - angleToPoint - 180;
		}
	}
	double angdist = angleToPoint - R;
	//fprintf(log_p,"Angle: %g dist: %g\n",angdist,dist);
	if(angdist > 180){
		angdist -= 360;
	}
	if(angdist < -180){
		angdist += 360;
	}
	//fprintf(log_p,"Angle: %g dist: %g 222222222\n",angdist,dist);
	//si pilota esta per darrera el robot
	//aquet invertira la seva direccio

	int signe=1;

	if(angdist > 90){
		angdist = 180 - angdist;
		signe = -1;
	}
	if(angdist < -90){
		angdist = -180 - angdist;
		signe = -1;
	}
	//fprintf(log_p,"Angle: %g dist: %g 3333333333\n",angdist,dist);
	double f2 = ka * angdist;
	double f1 = 0;

	double w = combinacio(f1,f2,dist,30);
	w = signe * w;
	
	f2 = 0;
	f1 = kd * dist;

	double vlin = combinacio(f1,f2,abs(angdist),45);
	vlin = signe * vlin;

	aplica(robot,vlin,w, env, qui);
}


void PredictBall ( Environment *env )
{
	double dx = env->currentBall.pos.x - env->lastBall.pos.x;
	double dy = env->currentBall.pos.y - env->lastBall.pos.y;
	env->predictedBall.pos.x = env->currentBall.pos.x + dx;
	env->predictedBall.pos.y = env->currentBall.pos.y + dy;

}

int quadrant(double angle){
	if (angle<0) angle+=360;
	if(angle>=0 && angle < 90){
		return 1;
	}else if(angle>=90 && angle < 180){
		return 2;
	}else if(angle>=180 && angle < 270){
		return 3;
	}else{
		return 4;
	}

}

// falta contemplar l'angle de rotaci�
int dinsTracada(Robot *b1, double Bx,double By,double Br, int qui, int ki2){
	double Ax = b1->pos.x;
	double Ay = b1->pos.y;
	double Ar = b1->rotation;
	double Av = (b1->velocityRight+b1->velocityLeft)/2;
	//double Avl = b1->velocityLeft;
	if(Ar<0)Ar+=360;
//	double Bx= b2->pos.x;
//	double By= b2->pos.y;
//	double Br = b2->rotation;
	
	//proxima posici� 
	double posx=0;
	double posy=0;
	int q = quadrant(Ar);
	switch(q){
		case 1:
			posx = Ax + (Av*0.2*cos(Ar*GRtoRAD));
			posy = Ay + (Av*0.2*sin(Ar*GRtoRAD));
			//fprintf(log_p,"%i++R1(%i): x: %g y: %g posx: %g posy: %g Angle: %g,   R2(%i): x: %g y: %g\n",q,qui,Ax,Ay,posx,posy,Ar,ki2,Bx,By);
			break;
		case 2:
			posx = Ax - (Av*0.2*cos((180-Ar)*GRtoRAD));
			posy = Ay + (Av*0.2*sin((180-Ar)*GRtoRAD));
			//fprintf(log_p,"%i++R1(%i): x: %g y: %g posx: %g posy: %g Angle: %g,   R2(%i): x: %g y: %g\n",q,qui,Ax,Ay,posx,posy,Ar,ki2,Bx,By);
			break;
		case 3:
			posx = Ax - (Av*0.*cos((Ar-180)*GRtoRAD));
			posy = Ay - (Av*0.2*sin((Ar-180)*GRtoRAD));
			//fprintf(log_p,"%i++R1(%i): x: %g y: %g posx: %g posy: %g Angle: %g Av: %g inc: %g ar-180: %g cos: %g,   R2(%i): x: %g y: %g\n",q,qui,Ax,Ay,posx,posy,Ar,Av,((Av*0.1)*cos(Ar-180)),(Ar-180),cos((Ar-180)*GRtoRAD),ki2,Bx,By);
			break;
		case 4:
			posx = Ax + (Av*0.2*cos((360-Ar)*GRtoRAD));
			posy = Ay - (Av*0.2*sin((360-Ar)*GRtoRAD));
			//fprintf(log_p,"%i++R1(%i): x: %g y: %g posx: %g posy: %g Angle: %g,   R2(%i): x: %g y: %g\n",q,qui,Ax,Ay,posx,posy,Ar,ki2,Bx,By);
			break;
	}
	
	
	if((b1->velocityLeft >= 0 && b1->velocityRight >= 0) || (b1->velocityLeft < 0 && b1->velocityRight < 0)){ // si tirem endavant
		if((posx <= Bx && Bx <= Ax)&&(posy <= By && By <= Ay) && (Ar>=180 && Ar < 270)){  //3r quadrant
			return 1;//3;
		}else if((posx <= Bx && Bx <= Ax)&&(posy >= By && By >= Ay) && (Ar>=90 && Ar < 180)){ //2n quadrant
			return 1;//2;
		}else if((posx >= Bx && Bx >= Ax) && (posy <= By && By <= Ay) && (Ar>=270 && Ar < 360)){ //4t quadrant
			return 1;//4;
		}else if((posx >= Bx && Bx >= Ax) && (posy >= By && By >= Ay) && (Ar>=0 && Ar < 90)){ //1r quadrant
			return 1;
		}else{
			return -1;
		}
		
	//}else if(b1->velocityLeft < 0 && b1->velocityRight < 0){ // endarrere
	//	if(((posx <= Bx && Bx <= Ax)&&(posy <= By && By <= Ay)) || ((posx >= Bx && Bx >= Ax) && (posy >= By && By >= Ay))){  //oest
	//		return 1;
	//	}else{
	//	}
		
	}else{// gira sobre ell mateix no tractar
		//fprintf(log_p,"++++++ROBOT: %i gira sobre ell mateix vl: %g vr: %g\n", qui, b1->velocityLeft, b1->velocityRight);
		return 0;
	}
	
	//double A2x = Ax + b1->
	//double A2y
}





void evitarAltresRobots(int qui, Environment *env){
	
	Robot *bot=&env->home[qui];
	int res=0;
	//1r evitem els nostres
	for (int i=0; i<PLAYERS_PER_SIDE; i++){// recorrem tots els robots
		if(i!=qui){
			Robot bot2 = env->home[i];
			res=dinsTracada(&env->home[qui], bot2.pos.x, bot2.pos.y,bot2.rotation,qui,i);
			switch(res){
				case 0: 
					//fprintf(log_p,"++++++ROBOT: %i gira sobre ell mateix vl: %g vr: %g\n", qui, env->home[qui].velocityLeft, env->home[qui].velocityRight);
					i=PLAYERS_PER_SIDE;
					break;
				case 1:
					if(bot->velocityLeft>10){
						bot->velocityLeft=0.25*bot->velocityLeft;
					}else if (bot->velocityLeft <0 ){
						bot->velocityLeft=2*bot->velocityLeft;
					}else{
						bot->velocityLeft=-10;
					}
					//fprintf(log_p,"++++++R: %i x: %g y: %g 1r q DINS la trajectoria de ROBOT: %i x: %g y: %g\n",i,bot2.pos.x, bot2.pos.y, qui,env->home[qui].pos.x, env->home[qui].pos.y);
					break;
				/*case 2:
					bot->velocityLeft=0.25*bot->velocityLeft;
					fprintf(log_p,"++++++R: %i x: %g y: %g 2n q DINS la trajectoria de ROBOT: %i x: %g y: %g\n",i,bot2.pos.x, bot2.pos.y, qui,env->home[qui].pos.x, env->home[qui].pos.y);
					break;
				case 3:
					bot->velocityLeft=0.25*bot->velocityLeft;
					fprintf(log_p,"++++++R: %i x: %g y: %g 3r q DINS la trajectoria de ROBOT: %i x: %g y: %g\n",i,bot2.pos.x, bot2.pos.y, qui,env->home[qui].pos.x, env->home[qui].pos.y);
					break;
				case 4:
					bot->velocityLeft=0.25*bot->velocityLeft;
					fprintf(log_p,"++++++R: %i x: %g y: %g 4t q DINS la trajectoria de ROBOT: %i x: %g y: %g\n",i,bot2.pos.x, bot2.pos.y, qui,env->home[qui].pos.x, env->home[qui].pos.y);
					break;*/
				default:
					break;
			}
		}
	}

	//2n evitem els contrincants

	for (int i=0; i<PLAYERS_PER_SIDE; i++){// recorrem tots els robots
		OpponentRobot bot2 = env->opponent[i];
		res=dinsTracada(&env->home[qui], bot2.pos.x, bot2.pos.y,bot2.rotation,qui,i+10);
		switch(res){
			case 0: 
				//fprintf(log_p,"++++++ROBOT: %i gira sobre ell mateix vl: %g vr: %g\n", qui, env->home[qui].velocityLeft, env->home[qui].velocityRight);
				i=PLAYERS_PER_SIDE;
				break;
			case 1:
				if(bot->velocityLeft>10){
						bot->velocityLeft=0.25*bot->velocityLeft;
					}else if (bot->velocityLeft <0 ){
						bot->velocityLeft=2*bot->velocityLeft;
					}else{
						bot->velocityLeft=-10;
					}
					//fprintf(log_p,"++++++R: %i x: %g y: %g 1r q DINS la trajectoria de ROBOT: %i x: %g y: %g\n",i,bot2.pos.x, bot2.pos.y, qui,env->home[qui].pos.x, env->home[qui].pos.y);
					break;
				//fprintf(log_p,"++++++OP: %i x: %g y: %g endavant DINS la trajectoria de ROBOT: %i x: %g y: %g\n",i+10,bot2.pos.x, bot2.pos.y, qui,env->home[qui].pos.x, env->home[qui].pos.y);
				break;
			case 2:
				//fprintf(log_p,"++++++OP: %i x: %g y: %g endarrere DINS la trajectoria de ROBOT: %i x: %g y: %g\n",i+10,bot2.pos.x, bot2.pos.y, qui,env->home[qui].pos.x, env->home[qui].pos.y);
				break;
			default:
				break;
		}
	}
}



int rotCorrectaPorter(double rot){ // la rot ha de ser -90 o 270
	if (rot < 0)rot+=360;
	if((rot > 267 && rot < 273) || (rot > 87 && rot < 93)){
		return 0;  //rot correcta
	}else{
		if(rot < 260 && rot > 80 ){
			return 1; // gir a l'esquerra
		}else{
			return 2; // gir a la dreta
		}
	}

}

Velocitats rotar(double desitjada, double rot, double Ax, double Ay, Environment *env){
	double desitjada2=0;
	if(desitjada > 180){
		desitjada2=desitjada -180;
	}
	//else{
	//	desitjada2=desitjada+180;
	//}
	double ls = desitjada + 10;
	double li = desitjada - 10;
	Velocitats v;
	double n= abs(desitjada-rot);
	if(n<180){ 
		
		v.vl=-(n/4);
		v.vr=n/4;
		fprintf(log_p,"AAAArot: %g desitjada: %g vl: %g vr: %g n: %g\n",rot, desitjada, n);
	}else{
		
		n=n-180;
		v.vl=n/4;
		v.vr=-(n/4);
		fprintf(log_p,"BBBBrot: %g desitjada: %g vl: %g vr: %g n: %g\n",rot, desitjada, n);
	}
		
	//}else if( Ax > env->fieldBounds.right-10){ // si estem a la defensa blava
	//	
	//}else if(Ax < env->fieldBounds.left+10){ // si estem a la defensa groga
		
	
	return v;
}


Velocitats sortirPorteria (double rot, double Ax, double Ay, Environment *env){
Velocitats v;
	if(Ax > MIGCAMPX){ // si estem a la poteria blava --> sortim!!!
		if(Ay > MIGCAMPY){
			if(rot > 120 && rot <= 300){ // endavant
				if(rot > 120 && rot <= 170){//girem un pel a l'esquerra
					v.vl=50;
					v.vr=40;
				}else if(rot > 250 && rot <= 300){ //girem una mica a la dreta
					v.vl=40;
					v.vr=50;
				}else{// recte
					v.vl=50;
					v.vr=50;
				}
			}else{ // endarrere
				if(rot > 70 && rot <= 120){
					v.vl=-40;
					v.vr=-50;
				}else if(rot > 300 && rot <= 350){ //girem una mica a la dreta
					v.vl=-50;
					v.vr=-40;
				}else{// recte 
					v.vl=-50;
					v.vr=-50;
				}
			}
		}else{
			if(rot > 60 && rot <= 240){ // endavant
				if(rot > 190 && rot <= 240){
					v.vl=50;
					v.vr=40;
				}else if(rot > 60 && rot <= 110){ 
					v.vl=40;
					v.vr=50;
				}else{// recte
					v.vl=50;
					v.vr=50;
				}
			}else{ // endarrere
				if(rot > 10 && rot <= 60){
					v.vl=-40;
					v.vr=-50;
				}else if(rot > 240 && rot <= 290){ 
					v.vl=-50;
					v.vr=-40;
				}else{// recte 
					v.vl=-50;
					v.vr=-50;
				}
			}
		}
		
	}else if(Ax < MIGCAMPX){ // si estem a la poteria groga -- > sortim

	}
	return v;
}

// si la pilota est� dins l'area xutar cap a fora
void Porter ( Robot *robot, Environment *env )
{	

	double velocityLeft = 0, velocityRight = 0;
	double rot=robot->rotation;
	if (rot<0)rot+=360;
	double Tx = env->currentBall.pos.x;
	double Ty = env->currentBall.pos.y;
	Velocitats v;
	double Ax = robot->pos.x;
	double Ay = robot->pos.y;
	/*fprintf(log_p,"Abans x: %g fins: %g y: %g fins: %g rot: %g \n",Ax,POSX_PORTER,Ay,POSY_PORTER,robot->rotation);*/
	fprintf(log_p,"rot: %g rot desitjada: 90 o 270 rotCorrecta %i\n",rot, rotCorrectaPorter(rot));
	if(Ax > (POSX_PORTER+1) || Ax < (POSX_PORTER-3) ){
		fprintf(log_p,"-------El porter s'ha mogut ax: %g rot: %g\n",Ax, rot);
		//vesaxy (robot, POSX_PORTER, POSY_PORTER, 1, env, 0);
		//vesaxy (robot, Tx, Ty, 0);
		if(Ax > (POSX_PORTER+1)){ // dins la porteria
			fprintf(log_p,"-------sortirPorteria ax: %g rot: %g\n",Ax, rot);
			v=sortirPorteria(rot, Ax, Ay,env);
			env->home[0].velocityLeft=v.vl;
			env->home[0].velocityRight=v.vr;
			//NearBound2 ( robot, velocityLeft , velocityRight, env ,0);
		}else{
			fprintf(log_p,"-------anem a xy posx: %g posy:%g ax: %g rot: %g\n",POSX_PORTER, POSY_PORTER,Ax, rot);
			vesaxy (robot, POSX_PORTER, POSY_PORTER, 1, env, 0);
		}
	}else if(0!=rotCorrectaPorter(rot)){
		if(rot<180){
			fprintf(log_p,"-------rotem 90 ax: %g rot: %g\n",Ax, rot);
			v= rotar(90, rot, Ax, Ay, env);
		}else{
			fprintf(log_p,"-------rotem 270 ax: %g rot: %g\n",Ax, rot);
			v =rotar(270, rot, Ax, Ay, env);
		}	
		NearBound2 ( robot, v.vl, v.vr, env ,0);
	}else{
		fprintf(log_p,"-------moviment normal ax: %g rot: %g\n",Ax, rot);
		//si el porter esta mes amunt q la pilota i mes amunt q la part inferior de la porteria
		if ( Ay > Ty + 0.9 && Ay >= 34 )  
		{
			velocityLeft = 100;//baixem
			velocityRight = 100;
		}

		//si el porter esta mes avall q la pilota i mes avall q la part superior de la porteria
		if ( Ay < Ty - 0.9 && Ay <= 50 )
		{
			velocityLeft = -100;//pugem
			velocityRight = -100;
		}

		if ( Ay < 34 ) //si ens passem de la porteria per baix
		{
			velocityLeft = -100;//pugem
			velocityRight = -100;
		}

		if ( Ay > 50 )// si ens passem per dalt
		{
			velocityLeft = 100;//baixem
			velocityRight = 100;
		}

	/*	double Tr = robot->rotation;
		if ( Tr < 0.001 )
			Tr = Tr + 360;
		if ( Tr > 360.001 )
			Tr = Tr - 360;
		if ( Tr > 270.5 )
			velocityLeft = velocityLeft + fabs ( Tr - 270 );
		else if ( Tr < 269.5 )
			velocityRight = velocityRight + fabs ( Tr - 270 );*/
		// si el robot no est� ben posicionat
		//robot->velocityLeft = velocityLeft;
		//robot->velocityRight = velocityRight;
		NearBound2 ( robot, velocityLeft , velocityRight, env ,0);
	}	
		
	
}



// defensar porteria
// falta controlar que no puguin entrar mai a dins la porteria
// si entren fer-los sortir
// si son a un canto que no empenin cap a la paret
// evitar amics
// si te la pilota el porter no anem contra la pilota
void Defend1 ( Robot *robot, Environment *env, double low, double high , int qui)
{
	double vl = 0, vr = 0;
	Vector3D z = env->currentBall.pos;
	Vector3D z2 = env->predictedBall.pos;

	double Tx = z.x;
	double Ty = z.y;
	double rot = robot ->rotation;
	Vector3D a = robot->pos;
	double Ax = a.x;
	double Ay = a.y;
	//a.x = env->goalBounds.right - a.x;
	//a.y = env->fieldBounds.top - a.y;
	
	fprintf(log_p,"ROBOT %i: x: %g, y: %g, rot: %g, POSX_DEF: %g \n",qui,a.x, a.y, rot, POSX_DEF);
	fprintf(log_p,"PILOTA: x: %g, y: %g, \n", Tx, Ty);
	if(a.x > env->fieldBounds.right-2 && a.y < env->goalBounds.top+4 && a.y > env ->goalBounds.bottom-4){ // si estem dins la porteria sortim
		fprintf(log_p,"ROBOT %i: dins la porteria\n",qui);
		Velocitats v=sortirPorteria(rot, Ax, Ay,env);
			env->home[0].velocityLeft=v.vl;
			env->home[0].velocityRight=v.vr;
	}else if(Tx > a.x || Tx > POSX_DEF){
		fprintf(log_p,"ROBOT %i: la pilota m'ha passat\n",qui);
		if(Ty > a.y){// si est� mes amunt de nosaltres ens posem per sota la pilota
			vesaxy(robot, ((Tx - a.x)/3)+a.x, Ty-4, 0,env,qui);
		}else if(Ty < a.y){ // si est� mes avall de nosaltres ens posem per damunt de la pilota
			vesaxy(robot, ((Tx - a.x)/3)+a.x, Ty+4, 0,env,qui);
		}
	}else if(Tx >= MIGCAMPX){
		fprintf(log_p,"ROBOT %i: xuto pilota\n",qui);
		vesaxy(robot,robot->pos.x,robot->pos.y,0,env,qui);


	}else if(a.x > POSX_DEF){
		fprintf(log_p,"ROBOT %i: la pilota esta endavant xo no estic a la meva posicio y: %g\n",qui,((high-low)/2)+low);
		vesaxy(robot, POSX_DEF, ((high-low)/2)+low, 1,env, qui);
	}else if(0!=rotCorrectaPorter(rot)){ // corregim rotacio
		Velocitats v;
		if(rot<180){
			fprintf(log_p,"-------rotem 90 ax: %g rot: %g\n",Ax, rot);
			v= rotar(90, rot, Ax, Ay, env);
		}else{
			fprintf(log_p,"-------rotem 270 ax: %g rot: %g\n",Ax, rot);
			v =rotar(270, rot, Ax, Ay, env);
		}	
		NearBound2 ( robot, v.vl, v.vr, env ,0);

	}else{
		fprintf(log_p,"ROBOT %i: defenso\n",qui);
		if ( a.y > Ty + 0.9 && a.y > low )
		{
			vl = 100;
			vr = 100;
		}
		if ( a.y < Ty - 0.9 && a.y < high )
		{
			vl = -100;
			vr = -100;
		}
		if ( a.y < low )
		{
			vl = -100;
			vr = -100;
		}
		if ( a.y > high )
		{
			vl = 100;
			vr = 100;
		}

		double Tr = robot->rotation;

		if ( Tr < 0.001 )
			Tr += 360;
		if ( Tr > 360.001 )
			Tr -= 360;
		if ( Tr > 270.5 )
			vr += fabs ( Tr - 270 );
		else if ( Tr < 269.5 )
			vl += fabs ( Tr - 270 );
	
		NearBound2 ( robot, vl ,vr, env ,qui);
	}
}


// Defensar el camp

void Defend2 ( Robot *robot, Environment *env, double low, double high ,int qui)
{
	double vl = 0, vr = 0;
	Vector3D z = env->predictedBall.pos;

	double Tx = z.x;
	if(z.x > FRIGHTX){
		z.x=FRIGHTX-0.5;
	}
	double Ty = z.y;
	Vector3D a = robot->pos;
	double rot = robot->rotation;
	//a.x = env->goalBounds.right - a.x;
	//a.y = env->fieldBounds.top - a.y;
	if(a.x > env->fieldBounds.right-2 && a.y < env->goalBounds.top+4 && a.y > env ->goalBounds.bottom-4){ // si estem dins la porteria sortim
		fprintf(log_p,"ROBOT %i: defenso22222 dins la porteria\n",qui);
		Velocitats v=sortirPorteria(rot, a.x, a.y,env);
			env->home[0].velocityLeft=v.vl;
			env->home[0].velocityRight=v.vr;
	}else if (a.x < POSX_DEF-5 || a.x > POSX_DEF+5){
		fprintf(log_p,"ROBOT %i: defenso22222 fora de posici�\n",qui);
		vesaxy(robot, a.x, a.y, 0,env,qui);

	}else if(Tx >= MIGCAMPX){
		fprintf(log_p,"ROBOT %i: defenso22222 xuto pilota\n",qui);
		vesaxy(robot,robot->pos.x,robot->pos.y,0,env,qui);


	}else{
		fprintf(log_p,"ROBOT %i: defenso22222 em moc pel meu camp\n",qui);
		if ( a.y > Ty + 0.9 && a.y > low )
		{
			vl = 100;
			vr = 100;
		}
		if ( a.y < Ty - 0.9 && a.y < high )
		{
			vl = -100;
			vr = -100;
		}
		if ( a.y < low )
		{
			vl = -100;
			vr = -100;
		}
		if ( a.y > high )
		{
			vl = 100;
			vr = 100;
		}

		double Tr = robot->rotation;

		if ( Tr < 0.001 )
			Tr += 360;
		if ( Tr > 360.001 )
			Tr -= 360;
		if ( Tr > 270.5 )
			vr += fabs ( Tr - 270 );
		else if ( Tr < 269.5 )
			vl += fabs ( Tr - 270 );

		NearBound2 ( robot, vl ,vr, env ,qui);
	}
}



// funci� per controlar i estem anant cap a un cant� i esquivar-lo, 
// per evitar fer for�a contra un costat

int controlCostats(Robot *bot, Environment *env){

	double Ax = bot->pos.x;
	double Ay = bot->pos.y;
	double Ar = bot->rotation;

	if(Ay >= env->fieldBounds.top-3 && (Ar >= 70 || Ar <= 110)){ // estem a la part superior
		vesaxy(bot,Ax,Ay-10,0, env, 10);
	}
	return 0;
}


void Attack1 ( Robot *robot, Environment *env ,int qui)
{
	Vector3D t = env->currentBall.pos;
	double r = robot->rotation;
	if ( r < 0 ) r += 360;
	if ( r > 360 ) r -= 360;
	double vl = 0, vr = 0;

	if ( t.y > env->fieldBounds.top - 2.5 ) t.y = env->fieldBounds.top - 2.5;
	if ( t.y < env->fieldBounds.bottom + 2.5 ) t.y = env->fieldBounds.bottom + 2.5;
	if ( t.x > env->fieldBounds.right - 3 ) t.x = env->fieldBounds.right - 3;
	if ( t.x < env->fieldBounds.left + 3 ) t.x = env->fieldBounds.left + 3;

	double dx = robot->pos.x - t.x;
	double dy = robot->pos.y - t.y;

	double dxAdjusted = dx;
	double angleToPoint = 0;

	if ( fabs ( robot->pos.y - t.y ) > 7 || t.x > robot->pos.x )
		dxAdjusted -= 5;

	if ( dxAdjusted == 0 )
	{
		if ( dy > 0 )
			angleToPoint = 270;
		else
			angleToPoint = 90;
	}
	else if ( dy == 0 )
	{
		if ( dxAdjusted > 0 )
			angleToPoint = 360;
		else
			angleToPoint = 180;
		
	}
	else
		angleToPoint = atan ( fabs ( dy / dx ) ) * 180.0 / PI;

	if ( dxAdjusted > 0 )
	{
		if ( dy > 0 )
			angleToPoint -= 180;
		else if ( dy < 0 )
			angleToPoint = 180 - angleToPoint;
	}
	if ( dxAdjusted < 0 )
	{
		if ( dy > 0 )
			angleToPoint = - angleToPoint;
		else if ( dy < 0 )
			angleToPoint = 90 - angleToPoint;
	}

	if ( angleToPoint < 0 ) angleToPoint = angleToPoint + 360;
	if ( angleToPoint > 360 ) angleToPoint = angleToPoint - 360;
	if ( angleToPoint > 360 ) angleToPoint = angleToPoint - 360;

	double c = r;

	double angleDiff = fabs ( r - angleToPoint );

	if ( angleDiff < 40 )
	{
		vl = 100;
		vr = 100;
		if ( c > angleToPoint )
			vl -= 10;
		if ( c < angleToPoint )
			vr -= 10;
	}
	else
	{
		if ( r > angleToPoint )
		{
			if ( angleDiff > 180 )
				vl += 360 - angleDiff;
			else
				vr += angleDiff;
		}
		if ( r < angleToPoint )
		{
			if ( angleDiff > 180 )
				vr += 360 - angleDiff;
			else
				vl += angleDiff;
		}
	}

	NearBound2 ( robot, vl, vr, env ,qui);
}

void Attack2 ( Robot *robot, Environment *env ,int qui)
{
	Vector3D t = env->currentBall.pos;
	double r = robot->rotation;
	if ( r < 0 ) r += 360;
	if ( r > 360 ) r -= 360;
	double vl = 0, vr = 0;

	if ( t.y > env->fieldBounds.top - 2.5 ) t.y = env->fieldBounds.top - 2.5;
	if ( t.y < env->fieldBounds.bottom + 2.5 ) t.y = env->fieldBounds.bottom + 2.5;
	if ( t.x > env->fieldBounds.right - 3 ) t.x = env->fieldBounds.right - 3;
	if ( t.x < env->fieldBounds.left + 3 ) t.x = env->fieldBounds.left + 3;

	double dx = robot->pos.x - t.x;
	double dy = robot->pos.y - t.y;

	double dxAdjusted = dx;
	double angleToPoint = 0;

	if ( fabs ( robot->pos.y - t.y ) > 7 || t.x > robot->pos.x )
		dxAdjusted -= 5;

	if ( dxAdjusted == 0 )
	{
		if ( dy > 0 )
			angleToPoint = 270;
		else
			angleToPoint = 90;
	}
	else if ( dy == 0 )
	{
		if ( dxAdjusted > 0 )
			angleToPoint = 360;
		else
			angleToPoint = 180;
		
	}
	else
		angleToPoint = atan ( fabs ( dy / dx ) ) * 180.0 / PI;

	if ( dxAdjusted > 0 )
	{
		if ( dy > 0 )
			angleToPoint -= 180;
		else if ( dy < 0 )
			angleToPoint = 180 - angleToPoint;
	}
	if ( dxAdjusted < 0 )
	{
		if ( dy > 0 )
			angleToPoint = - angleToPoint;
		else if ( dy < 0 )
			angleToPoint = 90 - angleToPoint;
	}

	if ( angleToPoint < 0 ) angleToPoint = angleToPoint + 360;
	if ( angleToPoint > 360 ) angleToPoint = angleToPoint - 360;
	if ( angleToPoint > 360 ) angleToPoint = angleToPoint - 360;

	double c = r;

	double angleDiff = fabs ( r - angleToPoint );

	if ( angleDiff < 40 )
	{
		vl = 100;
		vr = 100;
		if ( c > angleToPoint )
			vl -= 10;
		if ( c < angleToPoint )
			vr -= 10;
	}
	else
	{
		if ( r > angleToPoint )
		{
			if ( angleDiff > 180 )
				vl += 360 - angleDiff;
			else
				vr += angleDiff;
		}
		if ( r < angleToPoint )
		{
			if ( angleDiff > 180 )
				vr += 360 - angleDiff;
			else
				vl += angleDiff;
		}
	}

	NearBound2 ( robot, vl, vr, env ,qui);
}

void NearBound2 ( Robot *robot, double vl, double vr, Environment *env ,int qui)
{
	//Vector3D t = env->currentBall.pos;

	fprintf(log_p,"ROBOT %i: vl: %g, vr: %g \n",qui,vl,vr);
	Vector3D a = robot->pos;
	double r = robot->rotation;
	if(r<0)r+=360;
	

	if ( a.y > env->fieldBounds.top - 15 && r >= 45 && r <= 130 )
	{
		if ( vl > 0 )
			vl /= 3;
		if ( vr > 0 )
			vr /= 3;
	}

	if ( a.y < env->fieldBounds.bottom + 15 && r <= 315 && r >= 230 )
	{
		if ( vl > 0 ) vl /= 3;
		if ( vr > 0 ) vr /= 3;
	}

	if ( a.x > env->fieldBounds.right - 10 )
	{
		if ( vl > 0 )
			vl /= 2;
		if ( vr > 0 )
			vr /= 2;
	}

	if ( a.x < env->fieldBounds.left + 10 )
	{
		if ( vl > 0 )
			vl /= 2;
		if ( vr > 0 )
			vr /= 2;
	}
	fprintf(log_p,"ROBOT %i: vl: %g, vr: %g x: %g y: %g\n",qui,vl,vr,a.x,a.y);
	robot->velocityLeft = vr;
	robot->velocityRight = vl;

	evitarAltresRobots(qui, env);
}


